{-# LANGUAGE Trustworthy #-}

{- |
   It provides a secure interface for manipulation of references.
-}
module SecLib.Ref
    (
     -- * Secure references
       Ref ()
     -- * Operations
     , newRefSecIO
     , readRefSecIO
     , writeRefSecIO
    )

where

import SecLib.TCB.Ref
