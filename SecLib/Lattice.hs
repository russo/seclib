{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE Trustworthy #-}

{- |
This module describes security lattices.
-}

module SecLib.Lattice
    ( Less )
 where

import SecLib.TCB.Lattice
