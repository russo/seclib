{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE Unsafe #-}

module SecLib.TCB.Lattice where



class CanFlowTo l l' where

{- | The class 'Less' determines the allowed flows of information.  @Less l l'@
tests the order relationship of @l@ and @l'@ in the lattice, i.e., if @l@
&#8849; @l'@.

The sole purpose of the unexported class 'CanFlowTo' is to restrict
untrusted code from making arbitrary instances of the class 'Less' (credit to
Pablo Buiras!)  -}
class CanFlowTo l l' => Less l l' where
