{-# LANGUAGE Trustworthy #-}

-- | Provide security for computations involving pure values.
module SecLib.Sec
    (
     -- * Sec monad
      Sec ()
     -- * Upgrading security level
      , up
    )
where

import SecLib.TCB.Sec
