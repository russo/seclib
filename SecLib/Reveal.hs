{-# LANGUAGE Unsafe #-}

-- | Provide means for declassification.
module SecLib.Reveal
  ( reveal )
where

import SecLib.Lattice
import SecLib.TCB.Sec


{- | This function allows to downgrade information and it must be __used with care__.

Once that untrusted code gets access to this function (or any instance of it),
it can exercise it freely.

Normally, untrusted code gets access to a restricted versions of 'reveal'. For instance,
to reveal the last four digits of a credit card number, untrusted code gets access to the
following function.

@
reveal_4digits :: Less H L => Sec H CreditCard -> Sec L Int
@

-}
reveal :: Less l l' => Sec l' a -> Sec l a
reveal (MkSec a) = return a
