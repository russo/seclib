{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE Trustworthy #-}

-- | Provide security for computations involving side-effects.
module SecLib.SecIO
    (
     -- * SecIO monad
        SecIO ()
     -- * Lifting pure values
      , toSecIO
     -- * Execution
      , run
     -- * Composition
      , plug
    )

where

import SecLib.TCB.SecIO
