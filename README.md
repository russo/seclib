## A lightweight library for Information-Flow Control in Haskell

This is the repository for the SecLib package. When installed, SecLib provides
Haskell with static information-flow control (IFC). IFC allows untrusted code to
manipulate sensitive data (i.e., secret information), while preserving its
confidentiality.

The library is a modern version of the work presented in [A Library for
Light-weight Information-Flow Security in
Haskell](http://www.cse.chalmers.se/~russo/seclib.htm) by Alejandro Russo, Koen
Claessen and John Hughes, Proceedings of the ACM SIGPLAN 2008 Haskell Symposium.
